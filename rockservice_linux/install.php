<?php
/**
*	来自：信呼开发团队
*	作者：磐石(rainrock)
*	网址：http://www.rockoa.com/
*	信呼服务Linux版安装
*	帮助地址：http://www.rockoa.com/view_rockservice.html
*/


//nodejs的路径，如果你服务器已经有这个，请配置下面那文件
$nodepath 	= '/home/nodejs/node-v10.6.0-linux-x64/bin/node';




$base1		= str_replace('\\','/',dirname(__FILE__));
$name		= 'rockservice';
$strss 		= 'nohup '.$nodepath.' '.$base1.'/rockservice.js > '.$base1.'/'.$name.'_service.log 2>&1 &'."\n";	

$yun = '#!/bin/sh
cd '.$base1.'
NAME='.$name.'
PROCESS=`ps -ef|grep "$NAME"|grep -v grep|grep -v PPID|awk \'{ print $2}\'`
for i in $PROCESS
do
  echo "Kill the $NAME process [ $i ]"
  kill -9 $i
done
echo "stop $NAME"';

$yun1 = $yun.'
NOWDT=`date +"%Y-%m-%d.%H.%M.%S"`
'.$strss.'echo "start $NAME success"';

$pahts = ''.$base1.'/servicestart.sh';
file_put_contents($pahts, str_replace('^M','', $yun1));
if(!file_exists($pahts))exit('error create'.chr(10).'');
if(function_exists('exec'))exec('chmod 755 '.$pahts.'');

$pahts = ''.$base1.'/servicestop.sh';
file_put_contents($pahts, str_replace('^M','', $yun));
if(function_exists('exec'))exec('chmod 755 '.$pahts.'');

echo 'init install rockservice ok'.chr(10).'';