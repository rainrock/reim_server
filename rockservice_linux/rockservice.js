/**
*	信呼相关服务
*	开发者：雨中磐石(rainrock)
*	网址：http://www.rockoa.com/
*	时间：2019-07-17
*	运行：node rockservice.js
*/

var config		= require('./config.js');
var rockbase	= require('./core/rockbase.js');	
var rockqueue 	= require('./core/rockqueue.js');
var rockreim 	= require('./core/rockreim.js');

//基础服务
var baseobj		= new rockbase(config.getDebug());

//运行REIM服务端
var reimserver 	= new rockreim({
	'config'	: config.getREIMConfig(),
	'rockBase'	: baseobj
});
reimserver.run();

//开始运行队列的服务
new rockqueue({
	'config'	 	: config.getQueueConfig(),
	'rockBase' 		: baseobj,
	'reimserver'	: reimserver
}).run();