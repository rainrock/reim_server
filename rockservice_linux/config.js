/**
*	信呼service服务的配置文件
*	开发者：雨中磐石(rainrock)
*	网址：http://www.rockoa.com/
*	时间：2019-07-01
*	最后整理：2021-09-13
*/


var DEUBG 	= false;

var Config 	= {
	getDebug:function(){
		return DEUBG;
	},
	
	//队列推送服务的服务器和IP
	getQueueConfig:function(){
		return {
			'ip'   : '0.0.0.0', //对应服务IP，一般不要去修改
			'port' : 6553,		//如果这端口被占用就修改
		}
	},
	
	//REIM服务端的服务器和IP
	getREIMConfig:function(){
		return {
			'ip'    : '0.0.0.0',	//一般不要去修改
			'port'  : 6552,			//此端口需要对外开放
			'recid' : 'rockxinhu,rockkefu,reimplat,xinhuplat,xinhujava', 	//多个可以,分开
			'origin' : '*', 			//可连接的域名多个,连接,*或空不限制
			
			//[2021-09-13新增]是否使用ssl证书，也就是https,wss的加密传输，需要就改成true,
			'ssl_open': true,
			
			//证书key相对于根目录的文件如：F:/IIS/rockservice_windows_x64/crt/key.pem 必须是绝对路径
			'ssl_key' : '',
			
			//证书的地址如：F:/IIS/rockservice_windows_x64/crt/cert.crt
			'ssl_cert': '',	
			
			//证书的密码如果有就设置
			'ssl_pass': ''			
		}
	}
};

module.exports = Config;